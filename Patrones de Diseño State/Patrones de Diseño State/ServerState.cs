﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones_de_Diseño_State
{
    public abstract class ServerState
    {
        public abstract void Respuesta(); 
    }
}

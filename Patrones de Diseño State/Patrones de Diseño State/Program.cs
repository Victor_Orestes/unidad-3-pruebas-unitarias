﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones_de_Diseño_State
{
    class Program
    {
        static void Main(string[] args)
        {
            ServidorContext oServidor = new ServidorContext();
            oServidor.State = new DisponibleServerState();

            oServidor.AtenderSolicitud();

            oServidor.State = new SaturadoServerState();
            oServidor.AtenderSolicitud();
            
            oServidor.State = new SuperSaturadoServerState();
            oServidor.AtenderSolicitud();

            oServidor.State = new CaidoServerState();
            oServidor.AtenderSolicitud();
        }
    }
}

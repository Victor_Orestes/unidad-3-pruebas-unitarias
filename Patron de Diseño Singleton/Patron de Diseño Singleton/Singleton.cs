﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_de_Diseño_Singleton
{
    public class Singleton
    {
        private static Singleton instance = null;

        protected Singleton() { }
        public String mensaje = "UTH - ";

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                { instance = new Singleton(); }

                return instance;
            }
        }
    }
}

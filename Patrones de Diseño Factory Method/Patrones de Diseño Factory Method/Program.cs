﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones_de_Diseño_Factory_Method
{
    class Program
    {
        static void Main(string[] args)
        {
            BebidaEmbriagante oBebida = Creador.CreadorBedida(Creador.CERVEZA);
            Console.WriteLine(oBebida.CuentoMeEmbriagaPorHora());

            BebidaEmbriagante oBebida2 = Creador.CreadorBedida(Creador.VINO_TINTO);
            Console.WriteLine(oBebida2.CuentoMeEmbriagaPorHora());

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones_de_Diseño_Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal animal1 = new Animal() {nombre = "Obeja Dolly", patas = 4};
            Animal animal1Clonado = animal1.Clone() as Animal;
            animal1Clonado.patas = 5;

            Console.WriteLine(animal1.patas+" "+animal1.nombre);
            Console.WriteLine(animal1Clonado.patas+" "+animal1Clonado.nombre);
        }
    }
}
    

